/**
 * Created by Super_D_Luffy on 2017/5/23/023.
 */
public class Book {

    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (!name.equals(book.name)) return false;
        if (!author.equals(book.author)) return false;
        if (!PublishCompany.equals(book.PublishCompany)) return false;
        return PublishDate.equals(book.PublishDate);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + author.hashCode();
        result = 31 * result + PublishCompany.hashCode();
        result = 31 * result + PublishDate.hashCode();
        return result;
    }

    private String author;

    @Override
    public String toString() {
        return "Book: " + "\n--------------------------------------------------------------"+
                "\nname: 《" + name + "》"+
                "\nauthor: " + author  +
                "\nPublishCompany: " + PublishCompany  +
                "\nPublishDate: " + PublishDate  +
                "\npage: " + page +
                "\nmoney: " + money +
                "\n--------------------------------------------------------------";
    }

    public String getPublishCompany() {
        return PublishCompany;
    }

    public void setPublishCompany(String publishCompany) {
        PublishCompany = publishCompany;
    }

    public String getPublishDate() {
        return PublishDate;
    }

    public void setPublishDate(String publishDate) {
        PublishDate = publishDate;
    }

    private String PublishCompany;
    private String PublishDate;
    private int page;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    private double money;
}


