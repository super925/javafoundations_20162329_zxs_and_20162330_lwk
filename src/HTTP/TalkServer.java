package HTTP;

/**
 * Created by 蜡笔小新丶 on 2017/6/7.
 */

import Arithmetic4.ED2.MyDC;

import java.io.*;
import java.math.BigInteger;
import java.net.*;
import java.security.MessageDigest;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;



public class TalkServer {
    public static void main(String args[]) {
        MyDC counts = new MyDC();
        String Message ="";
        try {
            ServerSocket server = null;
            try {
                server = new ServerSocket(9005);
                System.out.println("连接成功！");
            //创建一个ServerSocket在端口4700监听客户请求
            } catch (Exception e) {
                System.out.println("can not listen to:" + e);
            //出错，打印出错信息
            }
            Socket socket = null;
            try {
                socket = server.accept();
            } catch (Exception e) {
                System.out.println("Error." + e);
            //出错，打印出错信息
            }
            String line;
            BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            //由Socket对象得到输入流，并构造相应的BufferedReader对象
            PrintWriter os = new PrintWriter(socket.getOutputStream());
            //由Socket对象得到输出流，并构造PrintWriter对象
            BufferedWriter bw = new BufferedWriter(new FileWriter("lwk_pub_key.txt"));
            //解密
            ObjectInputStream B=new ObjectInputStream(new FileInputStream("zxs_RSA_priv.txt"));
            RSAPrivateKey prk=(RSAPrivateKey)B.readObject( );
            BigInteger D=prk.getPrivateExponent();
            BigInteger N=prk.getModulus();
            //加密
            ObjectInputStream b=new ObjectInputStream(new FileInputStream("lwk_RSA_pub.txt"));
            RSAPublicKey pbk=(RSAPublicKey)b.readObject( );
            BigInteger e=pbk.getPublicExponent();
            BigInteger n=pbk.getModulus();
            MessageDigest m1 = MessageDigest.getInstance("MD5");
            //由系统标准输入设备构造BufferedReader对象
            line = is.readLine();
            while (!line.equals("OK")) {
                if (line.equals("File")) {
                    line = is.readLine();
                    while (!line.equals("Yes")) {
                        bw.write(line);
                        bw.newLine();
                        line = is.readLine();
                    }
                    bw.close();
                    line = is.readLine();
                } else {
                    System.out.println("服务器接收哈希值:" + line);
                    String Mi = line;
                    line = is.readLine();
                    String ctext = line;
                    BigInteger C = new BigInteger(ctext);
                    BigInteger M = C.modPow(D, N);
                    byte[] arr = M.toByteArray();
                    for (byte i : arr)
                        Message += (char) i;
                    System.out.println("解密结果:" + Message);
                    m1.update(Message.getBytes("UTF8"));
                    byte s1[] = m1.digest();
                    String result = "";
                    for (int i = 0; i < s1.length; i++) {
                        result += Integer.toHexString((0x000000ff & s1[i]) | 0xffffff00).substring(6);
                    }
                    if (result.equals(Mi)) {
                        String A = counts.evaluate(Message);
                        System.out.println("计算结果：" + A);
                        byte ptext[] = A.getBytes("UTF8");
                        BigInteger m = new BigInteger(ptext);
                        BigInteger c = m.modPow(e, n);
                        os.println(c.toString());
                        os.flush();
                        System.out.println("服务器器发送:" + c);
                        line = is.readLine();
                    }
                }
            }
            os.close(); //关闭Socket输出流
            is.close();//关闭Socket输入流
            socket.close(); //关闭Socket
            server.close(); //关闭ServerSocket
        } catch (Exception e) {
            System.out.println("Error:" + e);
        //出错，打印出错信息
        }
    }
}