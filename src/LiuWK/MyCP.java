package LiuWK;

import java.io.*;

/**
 * Created by Super_D_Luffy on 2017/6/10/010.
 */
public class MyCP {
    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            System.err.println("请按正确格式运行：java MyCP <type-of-radix> <input-file-path> <output-file-path>");
            //参数报错
            System.exit(1);
        }
        else {
            if (!(args[0].equals("-tx") || args[0].equals("-xt"))) {
                System.err.println("转换类型的参数错误：-tx or -xt");
                System.exit(1);
            }
            else {
                BufferedReader input = new BufferedReader(new FileReader(args[1]));
                BufferedWriter output = new BufferedWriter(new FileWriter(args[2]));

                String reader, writer = "";
                reader = input.readLine();  //定义reader值，便于循环判断

                if (args[0].equals("-tx")) {
                    //十转二
                    while (reader != null) {
                        String[] file = reader.split(" ");  //使用split方法将数字用空格分隔，并依次传入数组
                        for (int i = 0; i < file.length; i++)
                            writer += Integer.toBinaryString(Integer.parseInt(file[i])) + " ";
                            //toBinaryString()方法返回对应的无符号整数的二进制字符串
                        output.write(writer);
                        output.newLine();
                        writer = "";  //重定义，避免写入之前的字节
                        reader = input.readLine();
                    }
                }
                else if (args[0].equals("-xt")) {
                    //二转十
                    while (reader != null) {
                        String[] file = reader.split(" ");
                        for (int i = 0; i < file.length; i++)
                            writer += Integer.parseInt(file[i], 2) + " ";
                            //Integer.parseInt方法基数为2，则实现二转十
                        output.write(writer);
                        output.newLine();
                        writer = "";
                        reader = input.readLine();
                    }
                }
                input.close();
                output.close();
            }
        }
    }
}
