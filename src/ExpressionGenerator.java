/**
 * Created by Super_D_Luffy on 2017/6/1/001.
 */
import java.util.Random;
import java.io.IOException;
import java.io.FileWriter;

public class ExpressionGenerator {
    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            System.err.println("java ExpressionGenerator <number-of-expressions> <number-of-operator> <output-file-path>");
            System.exit(1);
        }

        int expr_num = Integer.parseInt(args[0]);
        int optr_num = Integer.parseInt(args[1]);
        String file_path = args[2];

        String digits = "123456789";
        String operators = "+-*/";

        Random rnd = new Random();
        StringBuffer expr = new StringBuffer();
        FileWriter fw = new FileWriter(file_path);

        int r1 = rnd.nextInt(100);
        for (int i = 0; i < expr_num; ++i) {
            expr.append(digits.charAt(++r1 % 9));
            int r2 = rnd.nextInt(100);
            for (int j = 0; j < optr_num; ++j) {
                expr.append(' ');
                expr.append(operators.charAt(++r2 % 4));
                expr.append(' ');
                expr.append(digits.charAt(++r1 % 9));
            }
            expr.append('\n');
            fw.write(expr.toString());
            expr.delete(0, expr.length());
        }
        fw.close();
    }
}
