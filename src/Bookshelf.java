/**
 * Created by Super_D_Luffy on 2017/5/23/023.
 */
public class Bookshelf {
    public static void main(String[] args) {
        Book doc = new Book();
        Book src = new Book();
        Book bin = new Book();

        doc.setName("Java Foundations 2nd");
        doc.setAuthor("John Lewis");
        doc.setPublishCompany("Electronic Industry");
        doc.setPublishDate("2014.5");
        doc.setPage(524);
        doc.setMoney(79.0);

        src.setName("Java Foundations 2nd");
        src.setAuthor("John Lewis");
        src.setPublishCompany("Electronic Industry");
        src.setPublishDate("2014.5");
        src.setPage(500);
        src.setMoney(29.0);

        bin.setName("English");
        bin.setAuthor("Mark Twain");
        bin.setPublishCompany("Electronic Industry");
        bin.setPublishDate("2016.5");
        bin.setPage(800);
        bin.setMoney(100.0);

        System.out.println(doc);
        System.out.println(src);
        System.out.println(bin);

        System.out.println("Compare: " + doc.equals(src));
        System.out.println("Compare: " + doc.equals(bin));
    }
}