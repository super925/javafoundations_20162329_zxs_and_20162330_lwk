package File_conversion;

import java.io.*;
import java.util.StringTokenizer;

/**
 * Created by 蜡笔小新丶 on 2017/6/8.
 */
public class MyCP {
    public static void main(String[] args)throws IOException {
        BufferedReader bf = new BufferedReader(new FileReader(args[1]));
        BufferedWriter bw = new BufferedWriter(new FileWriter(args[2]));
        String readline,writeline = "";
        readline = bf.readLine();
        if(args[0].equals("-tx")){
                while (readline != null) {
                    StringTokenizer tokenizer = new StringTokenizer(readline);
                    while (tokenizer.hasMoreTokens()) {
                        writeline += Integer.toBinaryString(Integer.parseInt(tokenizer.nextToken())) + " ";
                    }
                    bw.write(writeline);
                    bw.newLine();
                    readline = bf.readLine();
                }
            }else if(args[0].equals("-bx")){
                while (readline != null){
                    StringTokenizer tokenizer = new StringTokenizer(readline);
                    while(tokenizer.hasMoreTokens()){
                        writeline += Integer.parseInt(tokenizer.nextToken(),2) + " ";
                    }
                    bw.write(writeline);
                    bw.newLine();
                    readline = bf.readLine();
                }
            }
        bf.close();
        bw.close();
    }
}
