package Arithmetic4.ED2.IOtest;

import Arithmetic4.ED2.IOPractice;
import Arithmetic4.ED2.Practice;

import java.util.Scanner;

/*
    将题目写入文件的测试类
 */
public class InOutputTest {
    public static String language;

    public static void main(String[] args) {
        IOPractice iopra = new IOPractice();
        Scanner scan = new Scanner(System.in);
        String another = "y";

        System.out.print("选择语言 (Select language)\t\t\tA)简体中文\t\tB)English\t\tC)繁體中文" +
                "\n输入A,B或C (Input A,B,or C)：");
        language = scan.nextLine();

        while (true) {
            if (language.equalsIgnoreCase("A")) {
                System.out.print("输入需要题目的数量：");
                int number = scan.nextInt();
                while (true) {
                    try {
                        if (number > 0) {
                            System.out.print("\n【提醒：初始等级为1即（两个数的运算）每加一级多一个字符和一个数】\n\n请输入等级：");
                            int many = scan.nextInt();
                            iopra.Ti(number, many, language);
                            iopra.inFile("src/Arithmetic4/ED2/IOtest/Practice.txt");
                            break;
                        } else throw new Exception();
                    } catch (Exception e) {
                        System.out.println("题目数量输入错误，请重新输入（要求至少为1）");
                        number = scan.nextInt();
                    }
                }
                break;
            } else if (language.equalsIgnoreCase("B")) {
                System.out.print("The number of questions you need: ");
                int number = scan.nextInt();
                while (true)
                    try {
                        if (number > 0) {
                            System.out.print("\n【e.g. a + b is a level 1 and when adding a level," +
                                    "(meanwhile) increasing a digit and an operator】" +
                                    "\nThe level of questions you need:");
                            int many = scan.nextInt();
                            iopra.Ti(number, many, language);
                            iopra.inFile("src/Arithmetic4/ED2/IOtest/Practice.txt");
                            break;
                        } else throw new Exception();
                    } catch (Exception e) {
                        System.out.println("The number of questions is incorrect, Please re-enter it (at least 1)");
                        number = scan.nextInt();
                    }
                break;
            } else if (language.equalsIgnoreCase("C")) {
                System.out.print("輸入需要題目的數量：");
                int number = scan.nextInt();
                while (true)
                    try {
                        if (number > 0) {
                            System.out.print("\n【提醒：初始等級為1即（兩個數的運算）每加一級多一個字符和一個數】\n\n請輸入等級:");
                            int many = scan.nextInt();
                            iopra.Ti(number, many, language);
                            iopra.inFile("src/Arithmetic4/ED2/IOtest/Practice.txt");
                            break;
                        } else throw new Exception();
                    } catch (Exception e) {
                        System.out.println("題目數量輸入錯誤，請重新輸入（要求至少為1）");
                        number = scan.nextInt();
                    }
                break;
            } else
                while (!language.equalsIgnoreCase("A") && !language.equalsIgnoreCase("B") && !language.equalsIgnoreCase("C")) {
                    System.out.println("语言选择错误，请重新输入（A,B or C）");
                    language = scan.nextLine();
                }
        }
    }
}