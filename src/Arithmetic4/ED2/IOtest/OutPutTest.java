package Arithmetic4.ED2.IOtest;

import Arithmetic4.ED2.IOPractice;
import Arithmetic4.ED2.Practice;

import java.io.IOException;

/**
 * 将测试结果写入文件的测试类
 */
public class OutPutTest {
    public static void main(String[] args) {
        IOPractice pra = new IOPractice();
        try {
            pra.outFile("src/Arithmetic4/ED2/IOtest/Practice.txt", "src/Arithmetic4/ED2/IOtest/Score.txt");
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
