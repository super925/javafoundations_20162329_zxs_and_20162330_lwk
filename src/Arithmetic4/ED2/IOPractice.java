package Arithmetic4.ED2;

import Arithmetic4.ED2.Counts;
import Arithmetic4.ED2.Original;
import Arithmetic4.ED2.Practice;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.io.*;
import java.util.StringTokenizer;

/**
 * Created by 蜡笔小新丶 on 2017/5/20.
 */
public class IOPractice extends Practice {
    public IOPractice() {
        super();
    }

    public void inFile(String fileName) throws IOException {
        String Ti = "";
        int patnum = 0;
        BufferedWriter bf = new BufferedWriter(new FileWriter(fileName));
        for (String i : list) {
            patnum++;
            bf.write("第" + patnum + "题：");
            bf.newLine();
            Ti = i + "= ";
            bf.write(Ti);
            bf.newLine();
            bf.newLine();
        }
        bf.close();
    }

    //将题目判断结果写入文件
    public void outFile(String fileName1, String fileName2) throws IOException {
        Original ori = new Original();
        Counts count = new Counts();
        BufferedReader br = new BufferedReader(new FileReader(fileName1));
        BufferedWriter bf = new BufferedWriter(new FileWriter(fileName2));
        while (true) {
            String tmp = br.readLine();
            if (tmp != null) {
                if (tmp.indexOf("=") != -1) {
                    Tinum++;
                    StringTokenizer tokenizer = new StringTokenizer(tmp, "=");
                    ori.evaluate(tokenizer.nextToken());
                    list2.add(tmp);
                        if ((" " + count.evaluate(ori.getMessage())).equals(tokenizer.nextToken())) {
                            list2.add("正确！");
                            trues++;
                        } else {
                            list2.add("错误！");
                            list2.add("正确答案为：" + count.evaluate(ori.getMessage()));
                        }
                } else list2.add(tmp);
            } else break;
        }
        list2.add("总题数：" + Tinum);
        list2.add("正确总题数：" + trues);
        double accuracy = (double) trues / Tinum;
        list2.add("正确率：" + nf.format(accuracy));
        for (String i : list2) {
            bf.write(i);
            bf.newLine();
        }
        br.close();
        bf.close();
    }
}
