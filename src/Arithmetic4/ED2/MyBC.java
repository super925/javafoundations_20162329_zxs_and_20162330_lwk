package Arithmetic4.ED2;

import java.util.*;
/*
    中缀表达式向后缀表达式转换类。
 */

public class MyBC {
    private Stack<String> stack1;
    private List<String> list1;
    private String message;

    public MyBC() {
        stack1 = new Stack<String>();
        list1 = new ArrayList<String>();
    }

    //后缀表达式的转换方法
    public void evaluate(String expr) {
        String Message="";
        String token;
        StringTokenizer tokenizer = new StringTokenizer(expr);

        while (tokenizer.hasMoreTokens()) {
            token = tokenizer.nextToken();

            if (token.equals("("))
                stack1.push(token);
            else if (token.equals("+") || token.equals("-")) {
                while (!stack1.empty()){
                    if(stack1.peek().equals("(")){
                        break;
                    }else list1.add(stack1.pop());
                }
                stack1.push(token);
            }else if (token.equals("*") || token.equals("/")) {
                if(!stack1.empty()) {
                    if (stack1.peek().equals("*") || stack1.peek().equals("/")) {
                        list1.add(stack1.pop());
                        stack1.push(token);
                    } else stack1.push(token);
                }else stack1.push(token);
            }
            else if (token.equals(")")) {
                while (!stack1.peek().equals("(")) {
                    list1.add(stack1.pop());
                }
                stack1.pop();
            }else list1.add(token);
        }
        while (!stack1.empty()) {
            list1.add(stack1.pop());
        }
        for(String i:list1)
            Message += i+" ";
        list1.clear();
        message = Message;
    }

    //返回转换后的结果
    public String getMessage(){
        return message;
    }
}



