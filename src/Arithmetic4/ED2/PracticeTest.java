package Arithmetic4.ED2;

/*
    功能测试可，生成题目用户计算判断正误返回结果。
 */

import java.text.NumberFormat;
import java.util.*;

public class PracticeTest {
    public static void main(String[] args) {
        Practice pra = new Practice();
        Scanner scan = new Scanner(System.in);
        NumberFormat nf = NumberFormat.getPercentInstance();
        int Tinumber, Ticlass;
        String another = "y";
        String language;

        while (another.equalsIgnoreCase("y")) {
            System.out.print("\n选择语言 (Select language)\t\t\tA)简体中文\t\tB)English\t\tC)繁體中文" +
                    "\n输入A,B或C (Input A,B,or C)：");
            language = scan.nextLine();

            while (true) {
                if (language.equalsIgnoreCase("A")) {
                    System.out.print("\n您需要的题目数量：");
                    Tinumber = scan.nextInt();
                    while (true) {
                        if (Tinumber > 0) {
                            System.out.print("\n【提醒：初始等级为1即（两个数的运算）每加一级多一个字符和一个数】\n\n请输入等级：");
                            int many = scan.nextInt();
                            while (true) {
                                if (many > 0) {
                                    pra.Ti(Tinumber, many, language);
                                    pra.showC();
                                    System.out.println("\n答对的题目数：" + pra.getTrues());
                                    double trues = (double) pra.getTrues() / Tinumber;
                                    System.out.println("正确率为：" + nf.format(trues));
                                    System.out.println("\n再来一次？（输入y重来，输入n退出)");
                                    another = scan.nextLine();
                                    another = scan.nextLine();
                                    break;
                                } else
                                    System.out.println("级别输入错误，请重新输入（要求至少为1）");
                                many = scan.nextInt();
                            }
                            break;
                        } else
                            System.out.println("题目数量输入错误，请重新输入（要求至少为1）");
                        Tinumber = scan.nextInt();
                    }
                    break;
                } else if (language.equalsIgnoreCase("B")) {
                    System.out.print("\nThe number of questions you need: ");
                    Tinumber = scan.nextInt();
                    while (true) {
                        if (Tinumber > 0) {
                            System.out.print("\n【e.g. a + b is a level 1 and when adding a level," +
                                    "(meanwhile) increasing a digit and an operator】" +
                                    "\nThe level of questions you need:");
                            int many = scan.nextInt();
                            while (true) {
                                if (many > 0) {
                                    pra.Ti(Tinumber, many, language);
                                    pra.showE();
                                    System.out.println("\nThe number of the right answers: " + pra.getTrues());
                                    double trues = (double) pra.getTrues() / Tinumber;
                                    System.out.println("Accuracy: " + nf.format(trues));
                                    System.out.println("\nTest again?（input 'y' to start and 'n' to quit)");
                                    another = scan.nextLine();
                                    another = scan.nextLine();
                                    break;
                                } else
                                    System.out.println("The level of questions is incorrect, Please re-enter it (at least 1)");
                                many = scan.nextInt();
                            }
                            break;
                        } else
                            System.out.println("The number of questions is incorrect, Please re-enter it (at least 1)");
                        Tinumber = scan.nextInt();
                    }
                    break;
                } else if (language.equalsIgnoreCase("C")) {
                    System.out.print("\n您需要的題目數量：");
                    Tinumber = scan.nextInt();
                    while (true){
                        if (Tinumber > 0) {
                            System.out.print("\n\n【提醒：初始等級為1即（兩個數的運算）每加一級多一個字符和一個數】\n\n請輸入等級:");
                            int many = scan.nextInt();
                            while (true) {
                                if (many > 0) {
                                    pra.Ti(Tinumber, many, language);
                                    pra.showF();
                                    System.out.println("\n答對的題目數：" + pra.getTrues());
                                    double trues = (double) pra.getTrues() / Tinumber;
                                    System.out.println("正確率為：" + nf.format(trues));
                                    System.out.println("\n再來一次？（輸入y重來，輸入n退出)");
                                    another = scan.nextLine();
                                    another = scan.nextLine();
                                    break;
                                } else
                                    System.out.println("級別輸入錯誤，請重新輸入（要求至少為1）");
                                many = scan.nextInt();
                            }
                            break;
                        } else
                            System.out.println("級別輸入錯誤，請重新輸入（要求至少為1）");
                    Tinumber = scan.nextInt();
                }
                break;
            }
                else
                    System.out.println("语言选择错误，请重新输入（A,B or C）");
                    language = scan.nextLine();
            }
        }
    }
}




