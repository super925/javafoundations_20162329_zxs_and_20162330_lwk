package Arithmetic4.ED2;

import Arithmetic4.ED2.IOtest.InOutputTest;

import java.io.*;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.*;
import java.util.Random;

/**
 * Created by 蜡笔小新丶 on 2017/5/12.
 */
public class Practice {
    protected NumberFormat nf = NumberFormat.getPercentInstance();
    Random ran = new Random();
    private String sym, Ti = "";
    protected int Tinum = 0;
    protected int trues;;
    protected List<String> list;
    private List<String> list1;
    protected List<String> list2;

    public Practice() {
        list = new ArrayList<String>();
        list1 = new ArrayList<String>();
        list2 = new ArrayList<String>();
    }

    public void Ti(int number, int many,String language) {
        if(language.equalsIgnoreCase("A"))
            many = TiclassC(many);
        else if(language.equalsIgnoreCase("B"))
            many = TiclassE(many);
        else many = TiclassF(many);
        for (int j = 0; j < number; j++) {
            String ti = "";
            for (int i = 0; i < many; i++) {
                int A = ran.nextInt(20) + 1;
                int D = ran.nextInt(20) + 1;
                int B = ran.nextInt(5);
                int C = ran.nextInt(5);
                RationalNumber si = new RationalNumber(A, D);
                if (parity(i)) {
                    if (list1.indexOf("( ") == -1)
                        list1.add(getSym() + " ");
                    else if (list1.size() - list1.lastIndexOf("( ") > 4) {
                        if (list1.lastIndexOf(") ") - list1.lastIndexOf("( ") < 0 && B == 0) {
                            list1.add(") ");
                            list1.add(getSym() + " ");
                        } else list1.add(getSym() + " ");
                    } else list1.add(getSym() + " ");
                } else if (i == many - 1) {
                    if (list1.lastIndexOf("( ") - list1.lastIndexOf(") ") > 0) {
                        if (C == 0) {
                            list1.add(si.toString() + " ");
                            list1.add(") ");
                        } else {
                            list1.add(A + " ");
                            list1.add(") ");
                        }
                    } else if (C != 0)
                        list1.add(A + " ");
                    else list1.add(si.toString() + " ");
                } else if (i == 0) {
                    if (C != 0)
                        list1.add(A + " ");
                    else list1.add(si.toString() + " ");
                } else if (list1.lastIndexOf(") ") != -1) {
                    if (list1.lastIndexOf(") ") - list1.lastIndexOf("( ") > 0 && B == 0) {
                        list1.add("( ");
                        if (C != 0)
                            list1.add(A + " ");
                        else list1.add(si.toString() + " ");
                    } else if (C != 0)
                        list1.add(A + " ");
                    else list1.add(si.toString() + " ");
                } else if (list1.indexOf("( ") == -1 && B == 0) {
                    list1.add("( ");
                    if (C != 0)
                        list1.add(A + " ");
                    else list1.add(si.toString() + " ");
                } else if (C != 0)
                    list1.add(A + " ");
                else list1.add(si.toString() + " ");
            }
            for (String i : list1)
                ti += i;
            list1.clear();
            list.add(ti);
        }
    }

    public void showC() {
        trues = 0;
        Original ori = new Original();
        Counts coun = new Counts();
        Scanner scan = new Scanner(System.in);
        ListIterator<String> li = list.listIterator();
        while (li.hasNext()) {
            String A = li.next();
            li.remove();
            ori.evaluate(A);
            String result = coun.evaluate(ori.getMessage());

            System.out.print("\n" + A + " = ");
            String B = scan.next();
            if (result.equals(B)) {
                System.out.println("正确！");
                trues++;
            } else {
                System.out.println("错误！正确答案为：" + result);
            }
        }
    }
    public void showE() {
        trues = 0;
        Original ori = new Original();
        Counts coun = new Counts();
        Scanner scan = new Scanner(System.in);
        ListIterator<String> li = list.listIterator();
        while (li.hasNext()) {
            String A = li.next();
            li.remove();
            ori.evaluate(A);
            String result = coun.evaluate(ori.getMessage());

            System.out.print("\n" + A + " = ");
            String B = scan.next();
            if (result.equals(B)) {
                System.out.println("Congratulations,you got it!");
                trues++;
            } else {
                System.out.println("Sorry,the right answer is " + result);
            }
        }
    }
    public void showF() {
        trues = 0;
        Original ori = new Original();
        Counts coun = new Counts();
        Scanner scan = new Scanner(System.in);
        ListIterator<String> li = list.listIterator();
        while (li.hasNext()) {
            String A = li.next();
            li.remove();
            ori.evaluate(A);
            String result = coun.evaluate(ori.getMessage());

            System.out.print("\n" + A + " = ");
            String B = scan.next();
            if (result.equals(B)) {
                System.out.println("正確！");
                trues++;
            } else {
                System.out.println("錯誤！正確答案為：" + result);
            }
        }
    }

    public int getTrues() {
        return trues;
    }


    public String getSym() {
        int A = ran.nextInt(4);
        switch (A) {
            case 0:
                sym = "+";
                break;
            case 1:
                sym = "-";
                break;
            case 2:
                sym = "×";
                break;
            case 3:
                sym = "÷";
                break;
        }
        return sym;
    }

    public boolean parity(int num) {
        if (num % 2 == 1)
            return true;
        else
            return false;
    }

    public int TiclassC(int many) {
        Scanner scan = new Scanner(System.in);
        int A = 1;
            while (true) {
                try {
                    if (many > 0) {
                        for (int i = 0; i < many; i++) {
                            A += 2;
                        }
                        break;
                    } else throw new Exception();
                } catch (Exception e) {
                    System.out.println("级别输入错误，请重新输入（要求级别至少为1）");
                    many = scan.nextInt();
                }
            }
        return A;
    }


    public int TiclassE(int many) {
        Scanner scan = new Scanner(System.in);
        int A = 1;
        while (true) {
            try {
                if (many > 0) {
                    for (int i = 0; i < many; i++) {
                        A += 2;
                    }
                    break;
                } else throw new Exception();
            }
            catch (Exception e) {
                System.out.println("The level of questions is incorrect, Please re-enter it (at least 1)");
                many = scan.nextInt();
            }
        }
        return A;
    }
    public int TiclassF(int many) {
        Scanner scan = new Scanner(System.in);
        int A = 1;
        while (true) {
            try {
                if (many > 0) {
                    for (int i = 0; i < many; i++) {
                        A += 2;
                    }
                    break;
                } else throw new Exception();
            }
            catch (Exception e) {
                System.out.println("級別輸入錯誤，請重新輸入（要求級別至少為1）");
                many = scan.nextInt();
            }
        }
        return A;
    }
}

