package Arithmetic4.ED1;

/**
 * Created by Super_D_Luffy on 2017/5/13/014.
 */

public class CorrectJudgment {
    private static int trues = 0;

    public static void judgment(boolean same, String num1) {
        if (same) {
            trues++;
            System.out.println("正确！"+"\n");
        } else {
            System.out.println("错误！");
            System.out.println("正确结果为：" + num1+"\n");
        }
    }

    public static int getTrues() {
        return trues;
    }
}


